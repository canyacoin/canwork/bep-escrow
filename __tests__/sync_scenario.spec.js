const app = require('../config/config').app
const binance = require('../app/binance')
const utils = require('../app/utils')
const sync = require('../app/sync')
const fs = require('fs')

app.jobs = []
app.admins = []
app.state = true;
app.CHAIN_START = Date.now()-1000000000;
app.ESCROW_ADDRESS = 'BNBESCROW'

const testdata = JSON.parse(fs.readFileSync('./__mockData__/sync_scenarios.json'))

jest.setTimeout(10000);
const mockPrice = jest.spyOn(binance, "getPrice");
mockPrice.mockImplementation(async() =>{return 0.05});

const mockLogging = jest.spyOn(utils, "logger");
mockLogging.mockImplementation(() =>{return true});

describe('SYNC SCENARIOS', () => {

  beforeAll(async() => {
    return await binance.binanceConnect()
  });

  beforeEach(async() => {
    app.jobs = [];
    app.admins = [];
  });

  test('happy', async() => {
    const mockTx = jest.spyOn(binance, "getTransactions");
    mockTx.mockImplementation(async() =>{return testdata.happyin});
    await sync.syncState();
    expect(app.jobs[0]).toMatchObject(testdata.happyout);
  });

  test('refund', async() => {
    const mockTx = jest.spyOn(binance, "getTransactions");
    mockTx.mockImplementation(async() =>{return testdata.refundin});
    await sync.syncState();
    expect(app.jobs[0]).toMatchObject(testdata.refundout);
  });

});