const app = require('../config/config').app
const binance = require('../app/binance')
const utils = require('../app/utils')
const sync = require('../app/sync')
const fs = require('fs')

app.jobs = []
app.admins = []
app.state = true;
app.CHAIN_START = Date.now()-1000000000;
app.ESCROW_ADDRESS = 'BNBESCROW'

const testdata = JSON.parse(fs.readFileSync('./__mockData__/sync.json'))

jest.setTimeout(10000);
const mockPrice = jest.spyOn(binance, "getPrice");
mockPrice.mockImplementation(async() =>{return 0.05});

const mockLogging = jest.spyOn(utils, "logger");
mockLogging.mockImplementation(() =>{return true});

describe('SYNC TESTS', () => {

  beforeAll(async() => {
    return await binance.binanceConnect()
  });

  beforeEach(async() => {
    app.jobs = [];
    app.admins = [];
  });

  test('Escrow should be created as PENDING', async() => {
    const mockTx = jest.spyOn(binance, "getTransactions");
    mockTx.mockImplementation(async() =>{return testdata.escrow.input});
    await sync.syncState();
    expect(app.jobs[0]).toMatchObject(testdata.escrow.output);
  });

  test('Escrow value should move status to ACTIVE', async() => {
    const mockTx = jest.spyOn(binance, "getTransactions");
    mockTx.mockImplementation(async() =>{return testdata.price.input});
    await sync.syncState();
    expect(app.jobs[0]).toMatchObject(testdata.price.output);
  });

    test('Release command should be released & update to RELEASING ', async() => {
    const mockTx = jest.spyOn(binance, "getTransactions");
    mockTx.mockImplementation(async() => {return testdata.release.input});
    await sync.syncState();
    expect(app.jobs[0]).toMatchObject(testdata.release.output);
  });

  test('Disburse command should update status to DISBURSED', async() => {
    const mockTx = jest.spyOn(binance, "getTransactions");
    mockTx.mockImplementation(async() => {return testdata.disburse.input});
    await sync.syncState();
    expect(app.jobs[0]).toMatchObject(testdata.disburse.output);
  });

  test('Funding command should add admins', async() => {
    const mockTx = jest.spyOn(binance, "getTransactions");
    mockTx.mockImplementation(async() =>{return testdata.fund.input});
    await sync.syncState();
    expect(app.admins).toMatchObject(testdata.fund.output);
  });

  test('Funding fails if escrow is live', async() => {
    const mockTx = jest.spyOn(binance, "getTransactions");
    mockTx.mockImplementation(async() =>{return testdata.badfund.input});
    await sync.syncState();
    expect(app.admins).toMatchObject(testdata.badfund.output);
  });

});