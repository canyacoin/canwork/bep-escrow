# BEPScrow - Hedged escrow service for BEP2 tokens on Binance Chain

BEPScrow is a hedged escrow service for BEP2 tokens on Binance Chain. Currently for experimental purposes only.

### Project stack:

- Nodejs
- GitLab CI
- Heroku

### Prerequisites

```
yarn
node v8^
```

### Env variables

Create `.env` file and set the following variables.

```
ESCROW_ADDRESS=
ESCROW_PRIVKEY=
PORT=
CHAIN_WSURI=wss://testnet-dex.binance.org/api/ws/
CHAIN_APIURI=https://testnet-dex.binance.org/
CHAIN_NET=  // Binanace chain eg. testnet or mainnet
CHAIN_START=    // Where to scan binance /transfers api from in ms. eg. 1546300800000
BNBUSD_PAIR=BNB_BUSD-BD1 // For pricing assets
```

### Project Setup

```
yarn install
yarn test
yarn start
```

## CI/CD

GitLab CI

- Deploy

Main Branch:

- master
