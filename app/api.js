const express = require('express')
const utils = require('./utils')
const api = express();
const app = require('../config/config').app
const cors = require('cors')

api.use(cors())

api.get('/', (req,res) =>{
    let endpoints = {
        "all jobs": '/jobs',
        "active": '/jobs/active',
        "disbursed": '/jobs/disbursed',
        "refunded": '/jobs/refunded',
        "single Job": '/job/{jobNo}',
        "user Address": '/user/{address}',
        "users": '/users',
        "admins": '/admins'
        }
    res.send(endpoints)
})

api.get('/jobs', async(req,res) =>{
    let stats = utils.getSyncStats()
    let jobs = {
        status: (utils.syncing() ? 'SYNCING' : 'LIVE'),
        stats:{
            total: app.jobs.length,
            active: stats.escrow,
            disbursed:  stats.closed,
            refunded:   stats.refunded
        },
        jobs:app.jobs
    }
    res.send(jobs);
})

api.get('/jobs/active', async(req,res) =>{
    let stats = utils.getSyncStats()
    let jobs = {
        status: (utils.syncing() ? 'SYNCING' : 'LIVE'),
        stats:{
            total: app.jobs.length,
            active: stats.escrow,
            disbursed:  stats.closed,
            refunded:   stats.refunded
        },
        active: app.jobs.filter(x => x.status == 'ACTIVE')
    }
    res.send(jobs);
})

api.get('/jobs/disbursed', async(req,res) =>{
    let stats = utils.getSyncStats()
    let jobs = {
        status: (utils.syncing() ? 'SYNCING' : 'LIVE'),
        stats:{
            total: app.jobs.length,
            active: stats.escrow,
            disbursed:  stats.closed,
            refunded:   stats.refunded
        },
        disbursed: app.jobs.filter(x => x.status == 'DISBURSED')
    }
    res.send(jobs);
})

api.get('/jobs/refunded', async(req,res) =>{
    let stats = utils.getSyncStats()
    let jobs = {
        status: (utils.syncing() ? 'SYNCING' : 'LIVE'),
        stats:{
            total: app.jobs.length,
            active: stats.escrow,
            disbursed:  stats.closed,
            refunded:   stats.refunded
        },
        refunded: app.jobs.filter(x => x.status == 'REFUNDED')
    }
    res.send(jobs);
})

api.get('/job/:jobNo', async(req,res) =>{
    let job = app.jobs.filter(x => x.id == req.params.jobNo);
    if(job.length > 0){
        res.send(job[0]);
    } else {
        res.sendStatus(404)
    }
})

api.get('/user/:address', async(req,res) =>{
    let payer = app.jobs.filter(x =>  x.escrow.payer == req.params.address);
    let payee = app.jobs.filter(x =>  x.payout.payee == req.params.address);
    let jobs = payer.concat(payee);

    if(jobs.length > 0){
        res.send({
            "address": req.params.address,
            "stats":{
                "payee": payee.length,
                "payer": payer.length,
                "total": jobs.length
            },
            "jobs":jobs
        });
    } else {
        res.sendStatus(404)
    }
})

api.get('/users', async(req,res) =>{
    let payers = app.jobs.map(x =>  x.escrow.payer);
    let payees = app.jobs.map(x =>  x.payout.payee);
    let addresses = payers.concat(payees);
    let uniqueAddresses = [...new Set(addresses)]; 
    if(uniqueAddresses){
        res.send({
            "stats":{
                "users": uniqueAddresses.length
            },
            "users":uniqueAddresses
        }) 
    } else {
        res.sendStatus(404)
    }
})

api.get('/admins', async(req,res) =>{

    let admins = app.admins;
    if(admins && admins.length > 0){
        res.send({
            "stats":{
                "admins": admins.length
            },
            "admins":admins
        }) 
    } else {
        res.sendStatus(404)
    }
})

api.listen(app.EXPRESS_PORT)