/* Local Imports */
const app = require('../config/config').app;
const utils = require('./utils');
const binance = require('./binance');
const h = utils.h;

/**
 * Processes the payload received from the Binance websocket
 * @param  {Object} payload     The websocket message payload
 * Refer https://docs.binance.org/api-reference/dex-api/ws-streams.html#3-transfer
 */
const handlePayload = async(payload) => {
    var commands = {
        "ESCROW":       newEscrow,          // Incoming
        "RELEASE":      releaseEscrow,      // Incoming
        "DISBURSE":     closeEscrow,        // Outgoing
        "FUND":         fundService,        // Incoming
        "REFUND":       refundEscrow,       // Both
        "VALUE":        priceEscrow,        // Outgoing
        "NEWVALUE":     priceEscrow,        // Outgoing
        "INCREASE":     increaseEscrow      // Incoming
    }

    try{
        await utils.validatePayload(payload)
        let tx = utils.parseTransaction(payload)
        return await commands[tx.txMemo.command](tx);
    } catch(error){
        utils.logger(error);
        return error
    }
}


/* Increase the value of an escrow */
const increaseEscrow = async(tx) => {
    try{
        await utils.validateIncrease(tx)

        let jobIndex = utils.findIndex(tx.txMemo.args[0])
        app.jobs[jobIndex].increaseEscrow(tx)
        utils.logger('INCREASE_SUCCESS',tx.txMemo.args[0]);

        let tknPrice = await utils.getPrice(tx.txTkn)
        tx.txValue = (tknPrice * tx.txQty) + app.jobs[jobIndex].escrow.value;
        await binance.transferToken(tx,'price')
            .then(refund => utils.logger('PRICE_SENT',tx.txHash))
            .catch(error => utils.logger('PRICE_FAILED',tx.txHash))

        return app.jobs[jobIndex];
    }
    catch(error){
        utils.logger(error, tx.txHash);
        return error;
    }
}

/* Confirm the price of an escrow */
const priceEscrow = async(tx) => {
    try{
        await utils.validatePrice(tx)

        let jobIndex = utils.findIndex(tx.txMemo.args[0])
        app.jobs[jobIndex].updatePrice(tx)

        utils.logger('PRICE_SUCCESS',tx.txMemo.args[0]);
        return app.jobs[jobIndex];
    }
    catch(error){
        utils.logger(error, tx.txHash);
        return error;
    }
}


/* New escrow */
const newEscrow = async(tx) => {
    try {
        await utils.validateEscrow(tx)
        app.jobs.push(new Escrow(tx))
        utils.logger('ESCROW_SUCCESS',tx.txMemo.args[0]);

        let tknPrice = await utils.getPrice(tx.txTkn)
        tx.txValue = (tknPrice * tx.txQty).toFixed(4);
        await binance.transferToken(tx,'price')

        return app.jobs[utils.findIndex(tx.txMemo.args[0])]
    } catch (error) {
        utils.logger(error, tx.txHash);
        await binance.transferToken(tx,'refund')
            .then(refund => utils.logger('REFUND_SUCCESS',tx.txHash))
            .catch(error => utils.logger('REFUND_FAILED',tx.txHash))
        return error;
    }   
}

/* Refund request from admin/payee */
const refundEscrow = async(tx) => {
    try{
        if(utils.txFromEscrowService(tx)){ 
            let job = utils.findJob(tx.txMemo.args[0])     
            if(job && job.status == 'REFUNDING'){
                let jobIndex = utils.findIndex(tx.txMemo.args[0])
                app.jobs[jobIndex].finaliseRefund(tx)
                return app.jobs[jobIndex]
            } else { return true}
        } else {      
            let job = utils.findJob(tx.txMemo.args[0])
            if((utils.isAdmin(tx.txFrom) || job.payout.payee == tx.txFrom) && job && (job.status == 'ACTIVE' || job.status == 'PENDING')){
                try{
                    let jobIndex = utils.findIndex(tx.txMemo.args[0])
                    app.jobs[jobIndex].refund(tx)
                    let txHash = await binance.transferToken(app.jobs[jobIndex],'refund')
                    utils.logger('REFUND_SUCCESS',txHash);
                    return app.jobs[jobIndex];
                }
                catch(error){
                    utils.logger(error, tx.txHash);
                    return error;
                }
            } else {
                return utils.makeError('REFUND_ERROR')
            }
        }
    } catch(error){
        //console.log(error)
        return error;
    }
}

/* Fund the escrow service & add admins */
const fundService = async(tx) => {
    if(utils.countJobs()){
        return utils.makeError('FUND_LATE')
    } else {
        app.admins.push({address:tx.txFrom, hash:tx.txHash})
        utils.logger('FUND_SUCCESS',tx.txHash)
        for await (args of tx.txMemo.args){
            let thisAdmin = await binance.checkAddress(args);
            if(thisAdmin){
                app.admins.push({address:args, hash:tx.txHash})
                utils.logger('ADMIN_SUCCESS',args)
            }
        }
        return app.admins;
    }
}


/* Release an escrow */
const releaseEscrow = async(tx) => {
    try{
        await utils.validateRelease(tx)
        let jobIndex = utils.findIndex(tx.txMemo.args[0])

        // Get value if the job doesn't have one yet (in the case of syncing)
        if(!app.jobs[jobIndex].escrow.value){
            tx.txValue = 0;
            app.jobs[jobIndex].escrow.value = 0;
            app.jobs[jobIndex].release(tx)
        } else {
            tx.txValue = await utils.calcTkns(app.CAN_TOKEN, app.jobs[jobIndex].escrow.value)
            tx.txValue = Number(tx.txValue.toFixed(4))
            app.jobs[jobIndex].release(tx)
        }      

        let txHash = await binance.transferToken(app.jobs[jobIndex],'disburse',null)
        utils.logger('RELEASE_SUCCESS',tx.txMemo.args[0]);
        return app.jobs[jobIndex];
    }
    catch(error){
        utils.logger(error, tx.txHash);
        return error;
    }
}


/* Close out an escrow after being released */
const closeEscrow = async(tx) =>{
    try{
        let job = utils.findJob(tx.txMemo.args[0]);
        let jobIndex = utils.findIndex(tx.txMemo.args[0])

        if(!h.txIsIncoming(tx) && job && job.status == 'RELEASING'){
            app.jobs[jobIndex].close(tx); 
            return app.jobs[jobIndex];
        }
        return tx;
    } catch(e){
        console.log(e)
    }
}

/* Escrow Class */
var Escrow = function(tx) {
    let {txFrom, txMemo, txHash, txQty, txTkn } = tx;
    let time = tx.time

    /* Top Level */
    this.id =     txMemo.args[0],
    this.status = 'PENDING',

    /* Escrow */
    this.escrow = {
        amount: txQty,
        asset:  txTkn,
        value:  null,
        payer:  txFrom,
        time:   time,
        hash:   txHash,
    },

    /* Payout */
    this.payout = {
        amount: null,
        asset:  app.CAN_TOKEN, // all payouts made in CAN
        value:  null,
        payee:  tx.txMemo.args[1],
        time:   null,
        hash:   null,
    },

    /* Events */
    this.events = [{
        event:  'ESCROW',
        hash:   txHash,
        time:   time,
        amount: txQty,
        asset: txTkn
    }]
}


/* Release */
Escrow.prototype.release = function(tx){
    let {txFrom, txMemo, txHash, txQty, txTkn, txValue } = tx;
    let time = tx.time;

    this.status = 'RELEASING'
    this.payout.amount = txValue;
    this.payout.value = this.escrow.value;
    this.payout.time = null;
    this.payout.hash = null;
    this.events.push({
        event: 'RELEASE',
        hash: txHash,
        time: time
    })
}

/* Confirm Released */
Escrow.prototype.close = function(tx){
    let time = tx.time;
    let {txHash} = tx
    this.status = 'DISBURSED'
    this.payout.amount = tx.txQty;
    this.payout.time = time;
    this.payout.hash = txHash;
    this.events.push({
        event: 'DISBURSE',
        hash: txHash,
        time: time,
        asset: this.escrow.asset,
        amount: tx.txQty,
        value: this.payout.value
    })

}

/* Refund */
Escrow.prototype.refund = function(tx){
    let {txFrom, txMemo, txHash, txQty, txTkn } = tx;
    let time = tx.time

    this.status = 'REFUNDING'
    this.payout.amount = this.escrow.amount,
    this.payout.time = time;
    this.payout.hash = null;
    this.events.push({
        event: 'REFUNDREQ',
        hash: txHash,
        time: time,
        requestor: utils.isAdmin(tx.txFrom) ? 'ADMIN' : 'PAYEE'
    })
}

/* Confirm Refund */
Escrow.prototype.finaliseRefund = function(tx){
    let {txFrom, txMemo, txHash, txQty, txTkn } = tx;
    let time = tx.time

    this.status = 'REFUNDED'
    this.payout.hash = txHash;
    this.events.push({
        event: 'REFUND',
        hash: txHash,
        time: time,
        asset: txTkn,
        amount: txQty
    })
}

/* Update Price */
Escrow.prototype.updatePrice = function(tx){
    let {txFrom, txMemo, txHash, txQty, txTkn } = tx;
    let time = tx.time
    let value = Number(txMemo.args[1]);
    this.escrow.value = value;
    this.status = 'ACTIVE';
    this.events.push({
        event: 'VALUE',
        hash: txHash,
        time: time,
        value: value 
    })
}

/* Update Price */
Escrow.prototype.increaseEscrow = function(tx){
    let {txFrom, txMemo, txHash, txQty, txTkn } = tx;
    let time = tx.time
    this.escrow.amount = this.escrow.amount + txQty;
    this.events.push({
        event: 'INCREASE',
        hash: txHash,
        time: time,
        amount: txQty
    })
}

/* Poll backlog for transactions to process */
const pollBacklog = async() =>{
    setInterval(async()=> {
        if(h.serviceIsLive()){
            let thisPayload = app.backlog.shift();
            if(thisPayload){
                return await handlePayload(thisPayload, false)
            }
        }
    }, 2000);
}

/* EXPORTS */
exports.handlePayload = handlePayload;
exports.pollBacklog = pollBacklog;